#!/bin/bash

while read line; do
    line=$(echo $line | sed -E 's/#.*//g') #Remove comments
    line=$(echo $line | sed -E 's/^ *| *$//g') #Trim string
    if [[ "$line" == "" ]]; then
        continue
    fi

    line="${line/#\~/$HOME}"

    echo $line

    rsync -aR $line filesdir/
done < files

while read line; do
    line=$(echo $line | sed -E 's/#.*//g') #Remove comments
    line=$(echo $line | sed -E 's/^ *| *$//g') #Trim string
    if [[ "$line" == "" ]]; then
        continue
    fi

    line="${line/#\~/$HOME}"

    echo Ignoring $line

    rm -rf filesdir/$line
done < ignore

chmod -R 755 filesdir

[ -f perms ] && rm perms

while read file; do
    file=$(echo $file | sed -e "s/^.*filesdir//g") #Remove start of path
    stat -c "%n %a %U:%G" $file >> perms
done <<< $(find filesdir/*)

sort perms -o perms
