#!/bin/bash

while read line; do
    file=$(echo "$line" | cut -d' ' -f1)
    perm=$(echo "$line" | cut -d' ' -f2)
    ownr=$(echo "$line" | cut -d' ' -f3)

    sudo chmod "$perm" "filesdir$file"
    sudo chown "$ownr" "filesdir$file"
done < perms

for i in filesdir/*; do
    file=$(basename "$i")
    sudo rsync -Ha "filesdir/$file/" "/$file/"
done

ownr="$(whoami)"
sudo chmod -R 755 filesdir
sudo chown -R "$ownr:$ownr" filesdir
